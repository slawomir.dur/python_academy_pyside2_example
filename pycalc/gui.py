__all__ = ["App"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "gui"
__version__ = '1.0'


from PySide2.QtCore import Qt
from PySide2 import QtWidgets as QW
from pycalc.business_logic import Logic, LogicException


class App:
    def __init__(self, *args, **kwargs):
        self.app = QW.QApplication(*args, **kwargs)

        self.__createWindow()
        self.__createMainWindowLayout()
        self.__createField()
        self.__createButtons()

        self.logic = Logic()

    def __createWindow(self):
        self.__window = QW.QWidget()

    def __createMainWindowLayout(self):
        self.__mainWindowLayout = QW.QVBoxLayout()

    def __createField(self):
        self.__field = QW.QLineEdit()
        self.__field.setAlignment(Qt.AlignRight)
        self.__mainWindowLayout.addWidget(self.__field)

    def __createButtons(self):
        buttonsLabels = ['Cls', 'Bck', 'Undo', '', '7', '8', '9', '/', '4',
                         '5', '6', '*', '1', '2', '3', '-', '0', '.', '=', '+']

        pos = 0
        buttonsPosistions = [
            (0, 0), (0, 1), (0, 2), (0, 3),
            (1, 0), (1, 1), (1, 2), (1, 3),
            (2, 0), (2, 1), (2, 2), (2, 3),
            (3, 0), (3, 1), (3, 2), (3, 3),
            (4, 0), (4, 1), (4, 2), (4, 3)
        ]

        self.__buttonsLayout = QW.QGridLayout()

        for buttonLabel in buttonsLabels:
            button = QW.QPushButton(buttonLabel)
            button.clicked.connect(self.__buttonAction(buttonLabel))
            if (pos == 3):
                self.__buttonsLayout.addWidget(QW.QLabel(''), buttonsPosistions[pos][0], buttonsPosistions[pos][1])
            else:
                self.__buttonsLayout.addWidget(button, buttonsPosistions[pos][0], buttonsPosistions[pos][1])
            pos += 1

        self.__mainWindowLayout.addLayout(self.__buttonsLayout)

    def __buttonAction(self, button):
        def buttonAction():
            self.__onButtonClicked(button)

        return buttonAction

    def __onButtonClicked(self, button):
        if button == "=":
            try:
                result = self.logic.calculate(self.__field.text())
            except LogicException as le:
                errorDialog = QW.QMessageBox()
                errorDialog.setText("Error: {}".format(str(le)))
            else:
                self.__field.setText(str(result))
        elif button == "Cls":
            self.__field.setText("")
        elif button == "Bck":
            self.__field.setText(self.__field.text()[:-1])
        elif button == "Undo":
            try:
                restoredText = self.logic.getLastExpression()
            except LogicException:
                pass
            else:
                self.__field.setText(restoredText)
        else:
            text = self.__field.text()
            text += button
            self.__field.setText(text)

    def __call__(self):
        self.__window.setLayout(self.__mainWindowLayout)
        self.__window.show()
        return self.app.exec_()
