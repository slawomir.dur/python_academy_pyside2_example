__all__ = ["Logic"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "business_logic"
__version__ = '1.0'

import numexpr


class LogicException(BaseException):
    pass


class Logic:
    def __init__(self):
        self.__history = []
        self.__computeIsLastAction = False

    def calculate(self, expression):
        try:
            result = numexpr.evaluate(expression)
            self.__history.append(expression)
            self.__history.append(str(result))
            self.__computeIsLastAction = True
            return result
        except Exception as e:
            raise LogicException(str(e))

    def getLastExpression(self):
        try:
            if self.__computeIsLastAction:
                self.__history.pop()
                self.__computeIsLastAction = False
            return self.__history.pop()
        except IndexError:
            raise LogicException("History is empty!")
